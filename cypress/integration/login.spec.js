before(() => {
    cy.clearLocalStorage();
});

describe('Login test', () => {
    it('Visits my local host', () => {
        cy.visit('/');
        cy.get('input[name="email"]')
            .type('user.name@domain.com')
            .should('have.value', 'user.name@domain.com');

        cy.get('input[name="password"]')
            .type('mySecretPassword2019')
            .should('have.value', 'mySecretPassword2019');

        cy.get('input[type="submit"]')
        .click();

        cy.get('.user')
            .should('contain', 'user.name@domain.com');
    });
    it('Fails to login', () => {
        cy.visit('/');
        cy.get('input[name="email"]')
           .type('wrong.user@domain.com')
           .should('have.value', 'wrong.user@domain.com')

        cy.get('input[name="password"]')
            .type('wrongPassword')
            .should('have.value', 'wrongPassword')

        cy.get('input[type="submit"')
        .click();

        cy.get('.user')
            .should('contain', 'user.name@domain.com')
    }); 

});

